# To do list
## Fine Location permission



```javascript 
override fun onCreate(savedInstanceState: Bundle?) {
     val permissions = arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION)
    ActivityCompat.requestPermissions(this, permissions,0)
    
    }
```
- handle the configuration changes in the middle of permission request process .
- handle the case if the user rejected the permission request first time , and second time .
- show the dialog explaining the reason why you need the permission or the benefit for user if he grand the permission to you

if you need handle all the above cases , you can try this library https://github.com/permissions-dispatcher/PermissionsDispatcher

## ???
