package com.mokinys98.Erasmusu

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.gconfirmation.*
import kotlinx.android.synthetic.main.gjoin.*
import kotlinx.android.synthetic.main.main.*

class Gjoin : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState) // comment
        setContentView(R.layout.gjoin)

        gjoin.setOnClickListener{
            val intent = Intent (this, MainActivity::class.java)
            startActivity(intent)
        }

    }

}


